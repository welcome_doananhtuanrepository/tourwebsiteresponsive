
const menu=document.querySelector(".menu"),
faBars=document.querySelector(".menu .fa-bars"),
navbar=document.querySelector(".navbar")

menu.addEventListener("click",()=>{
    navbar.classList.toggle("active")
    faBars.classList.toggle('fa-times')
})

window.addEventListener("scroll",()=>{
    faBars.classList.remove('fa-times')
    navbar.classList.remove("active")
})